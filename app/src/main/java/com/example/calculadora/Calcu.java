package com.example.calculadora;

import java.io.Serializable;

public class Calcu implements Serializable {

    float num1;
    float num2;

    public Calcu(float num1, float num2) {
        this.num1 = num1;
        this.num2 = num2;
    }

    public Calcu (){

    }

    public float getNum1() {
        return num1;
    }

    public void setNum1(float num1) {
        this.num1 = num1;
    }

    public float getNum2() {
        return num2;
    }

    public void setNum2(float num2) {
        this.num2 = num2;
    }

    public float suma() {
        float res;

        res = this.num1 + this.num2;

        return res;
    }

    public float resta() {
        float res;

        res = this.num1 - this.num2;

        return res;
    }

    public float multip() {
        float res;

        res = this.num1 * this.num2;

        return res;
    }

    public float dvision() {
        float res;

        res = (this.num1 / this.num2);

        return res;
    }

}
