package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class CalculadoraActivity extends AppCompatActivity {

    private TextView Cal;
    private Button Limpiar;
    private Button Regresar;
    private Calcu calcu;
    private Button btnSuma;
    private Button btnRestar;
    private Button btnMultip;
    private Button btnDivi;
    private EditText Num1;
    private EditText Num2;
    private TextView lbLnombre;
    private TextView txtnom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora);

        Cal = (TextView) findViewById(R.id.Calc);
        Num1 = (EditText) findViewById(R.id.txtnum);
        Num2 = (EditText) findViewById(R.id.TxtNum);
        btnSuma = (Button) findViewById(R.id.Sum);
        btnRestar = (Button) findViewById(R.id.Rest);
        Limpiar =(Button) findViewById(R.id.btnLimpiar);
        Regresar = (Button) findViewById(R.id.btnRegresar);
        btnMultip = (Button) findViewById(R.id.Multip);
        btnDivi = (Button) findViewById(R.id.Divi);
        txtnom = (TextView) findViewById(R.id.lbsNombre);
        calcu = new Calcu();

        Bundle datos = getIntent().getExtras();
        String usuario= datos.getString(getString(R.string.nombre));
        txtnom.setText(usuario);

        btnSuma.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String num1 = Num1.getText().toString();
                String num2 = Num2.getText().toString();
                if (num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 1" ,
                            Toast.LENGTH_SHORT).show();
                }
                else if (num2.matches("")) {
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 2",
                            Toast.LENGTH_SHORT).show();
                }else {
                    float num12 = Float.valueOf(num1);
                    float num11 = Float.valueOf(num2);

                    calcu.setNum1(num12);
                    calcu.setNum2(num11);

                    String Suma =String.valueOf(calcu.suma());
                    Cal.setText(Suma);
                }
            }
        });

        btnRestar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String num1 = Num1.getText().toString();
                String num2 = Num2.getText().toString();
                if (num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 1" ,
                            Toast.LENGTH_SHORT).show();
                }
                else if (num2.matches("")) {
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 2",
                            Toast.LENGTH_SHORT).show();
                }else {
                    float num12 = Float.valueOf(num1);
                    float num11 = Float.valueOf(num2);

                    calcu.setNum1(num12);
                    calcu.setNum2(num11);

                    String restar =String.valueOf(calcu.resta());
                    Cal.setText(restar);
                }

            }
        });

        btnMultip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = Num1.getText().toString();
                String num2 = Num2.getText().toString();
                if (num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 1" ,
                            Toast.LENGTH_SHORT).show();
                }
                else if (num2.matches("")) {
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 2",
                            Toast.LENGTH_SHORT).show();
                }else {
                    float num12 = Float.valueOf(num1);
                    float num11 = Float.valueOf(num2);

                    calcu.setNum1(num12);
                    calcu.setNum2(num11);

                    String Multip =String.valueOf(calcu.multip());
                    Cal.setText(Multip);
                }
            }
        });

        btnDivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = Num1.getText().toString();
                String num2 = Num2.getText().toString();
                if (num1.matches("")){
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 1" ,
                            Toast.LENGTH_SHORT).show();
                }
                else if (num2.matches("")) {
                    Toast.makeText(CalculadoraActivity.this, "Falto capturar Numero 2",
                            Toast.LENGTH_SHORT).show();
                }else {
                    float num12 = Float.valueOf(num1);
                    float num11 = Float.valueOf(num2);

                    calcu.setNum1(num12);
                    calcu.setNum2(num11);

                    String Divis =String.valueOf(calcu.dvision());
                    Cal.setText(Divis);
                }
            }
        });

        Limpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String num1 = Num1.getText().toString();
                String num2 = Num2.getText().toString();

             if (num1.matches("")) {
                 Toast.makeText(CalculadoraActivity.this, "Ingrese numeros para poder limpiar",
                         Toast.LENGTH_SHORT).show();
             }else if (num2.matches("")){
                 Toast.makeText(CalculadoraActivity.this, "Ingrese numeros para poder limpiar",
                         Toast.LENGTH_SHORT).show();
             }else {
                 Num1.setText("");
                 Num2.setText("");
                 Cal.setText("");
             }
            }
        });

        Regresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
