package com.example.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Calcu calcu;
    private Button btnEntrar;
    private Button btnSalir;
    private TextView lbLnombre;
    private EditText txtnom;
    private EditText txtpas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEntrar = (Button) findViewById(R.id.btningresar);
        btnSalir = (Button) findViewById(R.id.btnSalir);
        txtnom = (EditText) findViewById(R.id.editxU);
        txtpas = (EditText) findViewById(R.id.editxtC);
        calcu = new Calcu();

        btnEntrar.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String usuario = txtnom.getText().toString();
                String Contra = txtpas.getText().toString();

                if (usuario.matches("")) {
                    Toast.makeText(MainActivity.this, "Se debe de ingresar un usuario", Toast.LENGTH_SHORT).show();
                } else if (Contra.matches("")) {
                    Toast.makeText(MainActivity.this, "Se debe de ingresar una contraseña", Toast.LENGTH_SHORT).show();
                } else {
                    if (usuario.matches(getString(R.string.user)) && Contra.matches(getString(R.string.pass))) {
                        Intent i = new Intent(MainActivity.this,CalculadoraActivity.class);
                        i.putExtra(getString(R.string.nombre), getString(R.string.nombre));
                        startActivity(i);
                    }
                    else {
                        Toast.makeText(MainActivity.this, "Usuario o contraseña incorrectos", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });



    }
}
